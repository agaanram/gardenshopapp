﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using GardenShop.Models;

namespace GardenShop
{
    public class GardenShop_Context : DbContext
    {
        public GardenShop_Context() : base("GardenShop_Context")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Konfiguracja klucza złożonego dla ProductListing
            modelBuilder.Entity<ProductListing>()
            .HasKey(pl => new { pl.ProductCode, pl.PurchaseId });
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<Customer> Customers { get ; set; }
        public DbSet<Delivery> Deliveries {get ;set; }
        public DbSet<Purchase> Purchases{ get; set; }
        public DbSet<ProductListing> ProductListings{ get; set; }
        public DbSet<Employee> Employees{ get; set; }
}
}
