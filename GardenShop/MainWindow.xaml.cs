﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GardenShop.Pages;

namespace GardenShop
{
    public partial class MainWindow : Window
    {
        private readonly GardenShop_Context _context;
        public MainWindow()
        {
            _context = new GardenShop_Context();
            InitializeComponent();
            Frame.NavigationService.Navigate(new WelcomePage());
        }

        private void EmployeeNavButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.NavigationService.Navigate(new EmployeesPage());
        }

        private void ProductsNavButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.NavigationService.Navigate(new ProductsPage(_context));
        }

        private void DeliveriesNavButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.NavigationService.Navigate(new DeliveriesPage());
        }

        private void PurchaseNavButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.NavigationService.Navigate(new PurchasePage(_context));
        }

        private void NewPurchaseNavButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.NavigationService.Navigate(new NewPurchasePage(_context));
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Frame.NavigationService.Navigate(new WelcomePage());
        }
    }
}
