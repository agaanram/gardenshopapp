﻿namespace GardenShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Street = c.String(nullable: false, maxLength: 60),
                        HouseNr = c.String(nullable: false, maxLength: 5),
                        PostalCode = c.String(nullable: false, maxLength: 6),
                        Country = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId);
            
            CreateTable(
                "dbo.Producers",
                c => new
                    {
                        ProducerId = c.Int(nullable: false, identity: true),
                        ProducerName = c.String(maxLength: 60),
                        PhoneNumber = c.String(maxLength: 12),
                        ProducerAddress_AddressId = c.Int(),
                    })
                .PrimaryKey(t => t.ProducerId)
                .ForeignKey("dbo.Addresses", t => t.ProducerAddress_AddressId)
                .Index(t => t.ProducerAddress_AddressId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductPrice = c.Double(nullable: false),
                        ProductAmount = c.Int(nullable: false),
                        AccesoryType = c.String(),
                        AccesoryDepartment = c.String(),
                        PlantName = c.String(),
                        PlantInfo = c.String(maxLength: 200),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Producer_ProducerId = c.Int(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Producers", t => t.Producer_ProducerId)
                .Index(t => t.Producer_ProducerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Producers", "ProducerAddress_AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Products", "Producer_ProducerId", "dbo.Producers");
            DropIndex("dbo.Products", new[] { "Producer_ProducerId" });
            DropIndex("dbo.Producers", new[] { "ProducerAddress_AddressId" });
            DropTable("dbo.Products");
            DropTable("dbo.Producers");
            DropTable("dbo.Addresses");
        }
    }
}
