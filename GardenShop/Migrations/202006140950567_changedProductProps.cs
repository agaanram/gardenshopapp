﻿namespace GardenShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedProductProps : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Products");
            DropColumn("dbo.Products", "ProductId");
            DropColumn("dbo.Products", "ProductPrice");
            DropColumn("dbo.Products", "ProductAmount");
            DropColumn("dbo.Products", "AccesoryType");
            DropColumn("dbo.Products", "AccesoryDepartment");
            DropColumn("dbo.Products", "PlantName");
            AddColumn("dbo.Products", "ProductCode", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Products", "UnitListPrice", c => c.Double(nullable: false));
            AddColumn("dbo.Products", "ProductName", c => c.String());
            AddColumn("dbo.Products", "NumberInStock", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "Category", c => c.String());
            AddPrimaryKey("dbo.Products", "ProductCode");
            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "PlantName", c => c.String());
            AddColumn("dbo.Products", "AccesoryDepartment", c => c.String());
            AddColumn("dbo.Products", "AccesoryType", c => c.String());
            AddColumn("dbo.Products", "ProductAmount", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "ProductPrice", c => c.Double(nullable: false));
            AddColumn("dbo.Products", "ProductId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Products");
            DropColumn("dbo.Products", "Category");
            DropColumn("dbo.Products", "NumberInStock");
            DropColumn("dbo.Products", "ProductName");
            DropColumn("dbo.Products", "UnitListPrice");
            DropColumn("dbo.Products", "ProductCode");
            AddPrimaryKey("dbo.Products", "ProductId");
        }
    }
}
