﻿namespace GardenShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAllClasses : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Producers", "ProducerAddress_AddressId", "dbo.Addresses");
            DropIndex("dbo.Producers", new[] { "ProducerAddress_AddressId" });
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        Nip = c.Int(),
                        CompanyName = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        telephone = c.String(),
                        CardNumber = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        PurchaseId = c.Int(nullable: false, identity: true),
                        PurchaseDate = c.DateTime(nullable: false),
                        Customer_CustomerId = c.Int(),
                        Employee_PersonId = c.Int(),
                    })
                .PrimaryKey(t => t.PurchaseId)
                .ForeignKey("dbo.Customers", t => t.Customer_CustomerId)
                .ForeignKey("dbo.Employees", t => t.Employee_PersonId)
                .Index(t => t.Customer_CustomerId)
                .Index(t => t.Employee_PersonId);
            
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        DeliveryId = c.Int(nullable: false),
                        DeliveryDate = c.DateTime(nullable: false),
                        Address = c.String(),
                        DeliveryStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DeliveryId)
                .ForeignKey("dbo.Purchases", t => t.DeliveryId)
                .Index(t => t.DeliveryId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        telephone = c.String(),
                        Salary = c.Double(nullable: false),
                        JobTitle = c.String(),
                        LoginCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.ProductListings",
                c => new
                    {
                        ProductCode = c.Int(nullable: false),
                        PurchaseId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        UnitSalePrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductCode, t.PurchaseId })
                .ForeignKey("dbo.Products", t => t.ProductCode, cascadeDelete: true)
                .ForeignKey("dbo.Purchases", t => t.PurchaseId, cascadeDelete: true)
                .Index(t => t.ProductCode)
                .Index(t => t.PurchaseId);
            
            CreateTable(
                "dbo.PlantFertilizers",
                c => new
                    {
                        Plant_ProductCode = c.Int(nullable: false),
                        Fertilizer_ProductCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Plant_ProductCode, t.Fertilizer_ProductCode })
                .ForeignKey("dbo.Products", t => t.Plant_ProductCode)
                .ForeignKey("dbo.Products", t => t.Fertilizer_ProductCode)
                .Index(t => t.Plant_ProductCode)
                .Index(t => t.Fertilizer_ProductCode);
            
            AddColumn("dbo.Products", "IsToxic", c => c.Boolean());
            DropColumn("dbo.Producers", "PhoneNumber");
            DropColumn("dbo.Producers", "ProducerAddress_AddressId");
            DropTable("dbo.Addresses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Street = c.String(nullable: false, maxLength: 60),
                        HouseNr = c.String(nullable: false, maxLength: 5),
                        PostalCode = c.String(nullable: false, maxLength: 6),
                        Country = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId);
            
            AddColumn("dbo.Producers", "ProducerAddress_AddressId", c => c.Int());
            AddColumn("dbo.Producers", "PhoneNumber", c => c.String(maxLength: 12));
            DropForeignKey("dbo.ProductListings", "PurchaseId", "dbo.Purchases");
            DropForeignKey("dbo.PlantFertilizers", "Fertilizer_ProductCode", "dbo.Products");
            DropForeignKey("dbo.PlantFertilizers", "Plant_ProductCode", "dbo.Products");
            DropForeignKey("dbo.ProductListings", "ProductCode", "dbo.Products");
            DropForeignKey("dbo.Purchases", "Employee_PersonId", "dbo.Employees");
            DropForeignKey("dbo.Deliveries", "DeliveryId", "dbo.Purchases");
            DropForeignKey("dbo.Purchases", "Customer_CustomerId", "dbo.Customers");
            DropIndex("dbo.PlantFertilizers", new[] { "Fertilizer_ProductCode" });
            DropIndex("dbo.PlantFertilizers", new[] { "Plant_ProductCode" });
            DropIndex("dbo.ProductListings", new[] { "PurchaseId" });
            DropIndex("dbo.ProductListings", new[] { "ProductCode" });
            DropIndex("dbo.Deliveries", new[] { "DeliveryId" });
            DropIndex("dbo.Purchases", new[] { "Employee_PersonId" });
            DropIndex("dbo.Purchases", new[] { "Customer_CustomerId" });
            DropColumn("dbo.Products", "IsToxic");
            DropTable("dbo.PlantFertilizers");
            DropTable("dbo.ProductListings");
            DropTable("dbo.Employees");
            DropTable("dbo.Deliveries");
            DropTable("dbo.Purchases");
            DropTable("dbo.Customers");
            CreateIndex("dbo.Producers", "ProducerAddress_AddressId");
            AddForeignKey("dbo.Producers", "ProducerAddress_AddressId", "dbo.Addresses", "AddressId");
        }
    }
}
