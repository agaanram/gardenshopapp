﻿namespace GardenShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedProducedIdInProduct : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "Producer_ProducerId", "dbo.Producers");
            DropIndex("dbo.Products", new[] { "Producer_ProducerId" });
            RenameColumn(table: "dbo.Products", name: "Producer_ProducerId", newName: "ProducerId");
            AlterColumn("dbo.Products", "ProducerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "ProducerId");
            AddForeignKey("dbo.Products", "ProducerId", "dbo.Producers", "ProducerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ProducerId", "dbo.Producers");
            DropIndex("dbo.Products", new[] { "ProducerId" });
            AlterColumn("dbo.Products", "ProducerId", c => c.Int());
            RenameColumn(table: "dbo.Products", name: "ProducerId", newName: "Producer_ProducerId");
            CreateIndex("dbo.Products", "Producer_ProducerId");
            AddForeignKey("dbo.Products", "Producer_ProducerId", "dbo.Producers", "ProducerId");
        }
    }
}
