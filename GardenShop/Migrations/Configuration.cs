﻿namespace GardenShop.Migrations
{
    using GardenShop.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GardenShop.GardenShop_Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "GardenShop_Context";
        }

        protected override void Seed(GardenShop_Context context)
        {

            var p1 = new Producer { ProducerId = 1, ProducerName = "PlantExtra" };
            var p2 = new Producer { ProducerId = 2, ProducerName = "MossCo" };
            var p3 = new Producer { ProducerId = 3, ProducerName = "Fiskars" };
            var p4 = new Producer { ProducerId = 4, ProducerName = "Ekodarpol" };
            context.Producers.AddOrUpdate(p1, p2, p3, p4);

            var plants = new List<Plant>
            {
                new Plant{ ProductCode=1, UnitListPrice=24.99, ProductName="Storczyk", Producer=p1, PlantInfo="Podlewac dwa razy w tygodniu", NumberInStock=23 },
                new Plant{ ProductCode=2, UnitListPrice=34.99, ProductName="Opuncja", Producer=p2, PlantInfo="Stanowisko sloneczne", NumberInStock=3 },
                new Plant{ ProductCode=3, UnitListPrice=4.99, ProductName="Fittonia", Producer=p2, PlantInfo="Nadaje sie do lasow w sloiku", NumberInStock=14 },
                new Plant{ ProductCode=4, UnitListPrice=27.99, ProductName="Ficus elastica", Producer=p1, PlantInfo="Podlewac umiarkowanie", NumberInStock=11 },
            };

            plants.ForEach(p => context.Products.AddOrUpdate(p));

            var accesories = new List<Accessory>
            {
                new Accessory{ ProductCode=5, UnitListPrice=54.99, ProductName="Sekator", Category="Nozyce", NumberInStock=11, Producer=p3},
                new Accessory{ ProductCode=6, UnitListPrice=74.99, ProductName="Nozyce duze", Category="Nozyce", NumberInStock=6, Producer=p3},
                new Accessory{ ProductCode=7, UnitListPrice=8.99, ProductName="Rekawice Bl", Category="Inne", NumberInStock=26, Producer=p3}
            };

            accesories.ForEach(a => context.Products.AddOrUpdate(a));
            context.SaveChanges();

            var fert = new Fertilizer { ProductCode = 8, UnitListPrice = 14.99, ProductName = "Biohummus", Producer = p4, IsToxic = false };
            context.Products.AddOrUpdate(fert);

            var indcustomers = new List<IndividualCustomer> {
                new IndividualCustomer{ CustomerId=1, CardNumber="12312341", FirstName="Bob", LastName="Brown", Telephone="304955204"},
                new IndividualCustomer{ CustomerId=2, CardNumber="12340901", FirstName="Kevin", LastName="King", Telephone="495827180"}
            };
            indcustomers.ForEach(c => context.Customers.AddOrUpdate(c));

            var companies = new List<Company>
            {
                new Company{ CustomerId=3, CompanyName="Ald", Nip=32112304},
                new Company{ CustomerId=4, CompanyName="BBB", Nip=12399213}
            };
            companies.ForEach(c => context.Customers.AddOrUpdate(c));

            var purchases = new List<Purchase>
            {
                new Purchase{ PurchaseId=1, PurchaseDate=DateTime.Now,  ProductListings=new List<ProductListing>
                    {
                    new ProductListing{ ProductCode=4, Quantity=7}
                    }
                }
            };

            purchases.ForEach(p => context.Purchases.AddOrUpdate(p));
            context.SaveChanges();
        }
    }
}
