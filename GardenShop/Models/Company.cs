﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public class Company : Customer
    {
        public int Nip { get; set; }
        public String CompanyName { get; set; }
        public override string ToString()
        {
            return $"Id: {CustomerId}, Firma: {CompanyName}, Nip: {Nip}";
        }
    }
}
