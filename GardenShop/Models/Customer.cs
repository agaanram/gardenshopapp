﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public abstract class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public ICollection<Purchase> Purchases { get; set; }
    }
}
