﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public class Delivery
    {
        [Key]
        public int DeliveryId { get; set; } 
        public DateTime DeliveryDate { get; set; }
        public double DeliveryCost {
            get 
            {
                return !Address.Contains("Warszawa") ? 20 : 40;
            }
        }
        public String Address { get; set; }
        public Status DeliveryStatus { get; set; }
        [Required]
        public Purchase Purchase { get; set; }
    }
    public enum Status
    {
        Paid,
        Shipped,
        Delivered
    }
}
