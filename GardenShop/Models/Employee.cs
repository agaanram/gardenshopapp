﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GardenShop.Models
{
    public class Employee : IPerson
    {
        [Key]
        public int PersonId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Telephone { get; set; }
        public double Salary { get; set; }
        public String JobTitle { get; set; }
        public int LoginCode { get; set; }

        public ICollection<Purchase> ManagedPurchases{get;set;}
    }
}
