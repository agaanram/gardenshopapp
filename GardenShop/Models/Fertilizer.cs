﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public class Fertilizer : Product
    {
        public bool IsToxic { get; set; }
        public ICollection<Plant> Plants { get; set; }

    }
}
