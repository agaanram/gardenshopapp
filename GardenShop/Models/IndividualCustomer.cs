﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public class IndividualCustomer : Customer, IPerson
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Telephone { get; set; }
        public String CardNumber { get; set; }
        public static int Discount { get; set; }

        public override string ToString()
        {
            return $"Id: {CustomerId}, {FirstName} {LastName}, Card: {CardNumber}";
        }
    }
}
