﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GardenShop.Models
{
    public class Plant : Product
    {
      
        [MaxLength(200)]
        public String PlantInfo { get; set; }
        public ICollection<Fertilizer> Fertilizers { get; set; }

    }
}
