﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GardenShop.Models
{
    public class Producer
    {
        [Key]
        public int ProducerId { get; set; }
        [MaxLength(60)]
        public String ProducerName { get; set; }

        public ICollection<Product> ProducedProducts { get; set;}
    }
}
