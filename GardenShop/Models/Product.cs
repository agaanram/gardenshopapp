﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GardenShop.Models
{
    public abstract class Product
    {
        [Key]
        public int ProductCode { get; set; }
        [Required]
        public double UnitListPrice { get; set; }
        public String ProductName { get; set; }
        public int NumberInStock { get; set; }
        public int ProducerId { get; set; }
        public Producer Producer { get; set; }
        public ICollection<ProductListing> ProductListings { get; set; }

        public Product()
        {
            ProductListings = new List<ProductListing>();
        }
        public override string ToString()
        {
            return $"{ProductName}, {UnitListPrice}zł, Kod: {ProductCode}";
        }
    }
}
