﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public class ProductListing
    {
        public int Quantity { get; set; }
        public double UnitSalePrice { get; set; }
        public double SubTotal
        {
            get
            {
                return Quantity*UnitSalePrice;
            }
        }
        public int ProductCode { get; set; }
        public Product Product { get; set; }
        public int PurchaseId { get; set; }
        public Purchase Purchase { get; set; }

       
        public override string ToString()
        {
            return $"{Product.ProductName}      {Quantity}      {UnitSalePrice}zł       {SubTotal}zł";
        }
    }
}
