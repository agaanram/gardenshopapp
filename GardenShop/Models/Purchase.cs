﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GardenShop.Models
{
    public class Purchase
    {
        [Key]
        public int PurchaseId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public ICollection<ProductListing> ProductListings { get; set; }
        public double PurchaseTotalCost
        {
            get {      
                return ProductListings.Sum(p => p.SubTotal);
            }
        }

        public Employee Employee { get; set; }
        public Delivery Delivery { get; set; }
        public Customer Customer { get; set; }

        public Purchase()
        {
            ProductListings = new List<ProductListing>();
        }
        public override string ToString()
        {
            return $"Id: {PurchaseId}, Data: {PurchaseDate}, Koszt: {PurchaseTotalCost}";
        }

    }
}
