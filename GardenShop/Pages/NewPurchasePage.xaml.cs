﻿using GardenShop.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GardenShop.Pages
{
   
    public partial class NewPurchasePage : Page
    {
        private readonly GardenShop_Context _context;
        private Purchase _purchase;
        private Product _product;
        private ObservableCollection<ProductListing> _listings;

        public NewPurchasePage(GardenShop_Context context)
        {
            InitializeComponent();
            _context = context;
            _purchase = new Purchase { PurchaseId=getNewPurchaseId(), PurchaseDate = DateTime.Now};
            _listings = new ObservableCollection<ProductListing>();
            ListingsListView.ItemsSource = _listings;
        }

        private void SearchProductByCodeButton_Click(object sender, RoutedEventArgs e)
        {
            if (ProductCodeInputTextBox != null)
            {
                int searched = Int32.Parse(ProductCodeInputTextBox.Text);
                Product product = _context.Products.SingleOrDefault(p => p.ProductCode == searched);
                if (product != null)
                {
                    _product = product;
                    FoundProductLabel.Content = product;
                    setCBRangeForProduct(product);
                }
                else
                {
                    FoundProductLabel.Content = "Nie znaleziono produktu";
                }
            }
        }

        //Metoda do generowania wartości dla ComboBoxa określającego dostępną liczbę produktów 
        private void setCBRangeForProduct(Product product)
        {
            IEnumerable<int> productQuant = Enumerable.Range(1, product.NumberInStock);
            ProductQuantityComboBox.ItemsSource = productQuant;
            ProductQuantityComboBox.SelectedIndex = 0;
        }

        //Metoda dodająca pozycję zamowienia (określony produkt w podanej liczności) do rozliczenia
        private void AddProductToPurchaseButton_Click(object sender, RoutedEventArgs e)
        {
            var quantity = ProductQuantityComboBox.SelectedIndex + 1;

            //Sprawdzenie czy produkt o takiej nazwie (jak dodawany) nie znajduje się już w pozycjach zamówienia
            if (_listings.Where(p=> p.Product.ProductName == _product.ProductName).Any())
            {
                //Jeśli produkt znajduje się już na liście to jest usuwany, a potem wstawiany z uaktualnioną ilością
                var sameListing = _listings.Where(p => p.Product.ProductName == _product.ProductName).SingleOrDefault();
                _listings.Remove(sameListing);
                sameListing.Quantity += quantity;
                _listings.Add(sameListing);
            } else
            {
                //Jeśli pozycja nie znajduje się na liście to dodawana jest nowa
                var listing = new ProductListing { Product = _product, Quantity = quantity, UnitSalePrice = _product.UnitListPrice };
                _listings.Add(listing);
            }
            //Odjęcie liczby produktów od tych dostępnych na stanie
            _product.NumberInStock -= quantity;
            setCBRangeForProduct(_product);
            TotalSumLabel.Content = "" + _listings.Sum( l => l.SubTotal) + "zł";
        }

        //Metoda finalizująca rozliczenie, które tworzy powiązania między rozliczeniem a pozycjami rozliczenia
        //i zapisuje rozliczenie (z powiązaniami) do bazy
        private void AddNewPurchaseButton_Click(object sender, RoutedEventArgs e)
        {
            if (_purchase != null && _listings!= null && _listings.Count > 0)
            {
                _purchase.ProductListings = _listings;
                _context.Purchases.Add(_purchase);
                _context.SaveChanges();
                NavigationService.Navigate(new NewPurchasePage(_context));
            } else
            {
                AlertTextBlock.Text = "Należy dodać produkty!";
            }
        }

        //Metoda odświeżająca stronę i zerująca wszystkie wartości
        private void DeleteNewPurchaseButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new NewPurchasePage(_context));
        }
       
        //Metoda zwracająca id dla nowego rozliczenia
        private int getNewPurchaseId()
        {
            var count = _context.Purchases.Count();
            count++;
            return count;
        }

        //Metoda zerująca wartość TextBoxa do wpisywania kodu szukanego produktu 
        private void ProductCodeInputTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ProductCodeInputTextBox.Text = "";
        }

        //Metoda ustawiająca zawartość ComboBoxa z klientami na IndywidualnychKlientow
        private void PersonRadioButton_Click(object sender, RoutedEventArgs e)
        {
            var customers = _context.Customers.OfType<IndividualCustomer>().ToList();
            ClientsComboBox.ItemsSource = customers;
            ClientsComboBox.SelectedIndex = 0;
        }

        //Metoda ustawiająca zawartość ComboBoxa z klientami na Firmy
        private void CompanyRadioButton_Click(object sender, RoutedEventArgs e)
        {
            var customers = _context.Customers.OfType<Company>().ToList();
            ClientsComboBox.ItemsSource = customers;
            ClientsComboBox.SelectedIndex = 0;
        }

        //Metoda przypisująca (wybranego z ComboBoxa) Klienta do rozliczenia
        private void SetClientButton_Click(object sender, RoutedEventArgs e)
        {
            Customer customer = (Customer)ClientsComboBox.SelectedItem;
            if (customer != null)
            {
                _purchase.Customer = customer;
            }
        }
    }
}


