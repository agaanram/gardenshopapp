﻿using GardenShop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GardenShop
{
    
    public partial class ProductsPage : Page
    {
        private readonly GardenShop_Context _context;
        public ProductsPage(GardenShop_Context context)
        {
            InitializeComponent();
            _context = context;
        }

        private void PlantsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var plants = _context.Products.OfType<Plant>().Include(p=> p.Producer).ToList();
            PlantsListBox.ItemsSource = plants;
        }

        private void AccessoriesListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var acc = _context.Products.OfType<Accessory>().Include(p => p.Producer).ToList();
            AccessoriesListBox.ItemsSource = acc;
        }

        private void FertilizersListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var fert = _context.Products.OfType<Fertilizer>().Include(p => p.Producer).ToList();
            FertilizersListBox.ItemsSource = fert;
        }
    }
}
