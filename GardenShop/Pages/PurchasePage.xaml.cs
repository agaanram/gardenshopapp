﻿using GardenShop.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;


namespace GardenShop.Pages
{
    public partial class PurchasePage : Page
    {
        private readonly GardenShop_Context _context;

        public PurchasePage(GardenShop_Context context)
        {
            InitializeComponent();
            _context = context;
        }

        private void AllPurchasesListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var purchases = _context.Purchases.Include(p => p.ProductListings.Select(pr => pr.Product)).ToList();
            AllPurchasesListBox.ItemsSource = purchases;
        }

        private void AllPurchasesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Purchase selectedPurchase = (Purchase)AllPurchasesListBox.SelectedItem;
            NavigationService.Navigate(new SpecificPurchasePage(_context, selectedPurchase));
        }
    }
}


