﻿using GardenShop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GardenShop.Pages
{

    public partial class SpecificPurchasePage : Page
    {
        private readonly GardenShop_Context _context;

        public SpecificPurchasePage(GardenShop_Context context, Purchase selectedPurchase)
        {
            _context = context;
            InitializeComponent();
            SpecificPurchaseLabel.Content = $"Zakup nr: {selectedPurchase.PurchaseId}";
            IEnumerable<ProductListing> listings = selectedPurchase.ProductListings;
            SpecificPurchaseListView.ItemsSource = listings;
        }

        private void BackToPurchasesButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PurchasePage(_context));
        }
    }
}
